package main

import (
	"testing"
)

func TestSearch(t *testing.T) {
	filter := ""
	args := []string{"openjdk"}
	if err := Search(&filter, args); err != nil {
		t.Fatal("failed")
	}
}

func TestSearchFilter(t *testing.T) {
	filter := "alpine"
	args := []string{"openjdk"}
	if err := Search(&filter, args); err != nil {
		t.Fatal("failed")
	}
}
