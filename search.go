package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"sort"
	"strings"
	"sync"
)

var (
	DOCKER_HUB_REGISTRY_URL_TEMPLATE = "https://registry.hub.docker.com/v2/repositories/%s/tags/?page=%d"
	ITEM_PER_PAGE                    = 10
)

type Tag struct {
	Count    int         `json:"count"`
	Next     string      `json:"next"`
	Previous string      `json:"previous"`
	Results  []TagResult `json:"results"`
}

type TagResult struct {
	Name        string           `json:"name"`
	FullSize    int              `json:"full_size"`
	Id          int              `json:"id"`
	Repository  int              `json:"repository"`
	Creator     int              `json:"creator"`
	LastUpdater int              `json:"last_updater"`
	LastUpdated string           `json:"last_updated"`
	IamgeId     string           `json:"image_id"`
	V2          bool             `json:"v2"`
	Images      []TagResultImage `json:"images"`
}

type TagResultImage struct {
	Size         int    `json:"size"`
	Architecture string `json:"architecture"`
	Variant      string `json:"variant"`
	Features     string `json:"features"`
	Os           string `json:"os"`
	OsVersions   string `json:"os_versions"`
	OsFeatures   string `json:"os_features"`
}

func getTag(url string) (*Tag, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("http.Get error: url=%s, error=%s", url, err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("ioutil.ReadAll error: url=%s, error=%s", url, err)
	}
	tagResponse := new(Tag)
	err = json.Unmarshal([]byte(body), &tagResponse)
	return tagResponse, err
}

func maxCount(imageTag string) (int, error) {
	url := fmt.Sprintf(DOCKER_HUB_REGISTRY_URL_TEMPLATE, imageTag, 1)
	tagResponse, err := getTag(url)
	if err != nil {
		return -1, fmt.Errorf("getTag error: imageTag=%s, error=%s", imageTag, err)
	}
	return tagResponse.Count, nil
}

func Search(filter *string, args []string) error {
	imageTag := args[0]
	if !strings.Contains(imageTag, "/") {
		imageTag = fmt.Sprintf("library/%s", imageTag)
	}
	count, err := maxCount(imageTag)
	if err != nil {
		return err
	}
	filterPattern := regexp.MustCompile("^.+$")
	if *filter != "" {
		filterPattern = regexp.MustCompile(*filter)
	}
	result := make([]string, 0)
	tagStream := make(chan string, ITEM_PER_PAGE)
	done := make(chan struct{})
	go func(src <-chan string, dst *[]string, done chan struct{}) {
		for tag := range src {
			if filterPattern.MatchString(tag) {
				// log.Printf("tag: %s\n", tag)
				*dst = append(*dst, tag)
			}
		}
		close(done)
	}(tagStream, &result, done)
	var wg sync.WaitGroup
	for page := 1; page <= (count / ITEM_PER_PAGE); page++ {
		wg.Add(1)
		go func(page int, dst chan<- string, wg *sync.WaitGroup) {
			defer wg.Done()
			pageUrl := fmt.Sprintf(DOCKER_HUB_REGISTRY_URL_TEMPLATE, imageTag, page)
			tagResponse, err := getTag(pageUrl)
			if err != nil {
				log.Printf("getTag: pageUrl=%s, error=%s\n", pageUrl, err)
			} else {
				for _, tagResult := range tagResponse.Results {
					dst <- tagResult.Name
				}
			}
		}(page, tagStream, &wg)
	}
	wg.Wait()
	close(tagStream)
	<-done

	sort.Slice(result, func(i int, j int) bool {
		return result[i] < result[j]
	})
	for _, s := range result {
		fmt.Println(s)
	}
	return nil
}
