docker-tag-search
====

## Build

```shell
go build
```

## Usage

```console
Usage: docker-tag-search [flags] <repository name>

flags:

  -filter string
        filtering pattern
```

### `<repository name>`

Docker repository name

### `-filter`

Regexp
