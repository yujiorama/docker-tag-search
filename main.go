package main

import (
	"flag"
	"fmt"
	"log"
	"os"
)

func main() {
	fs := flag.NewFlagSet("docker-tag-search", flag.ExitOnError)
	// filter := fs.String("filter", "", "filter")
	fs.Usage = func() {
		fmt.Println("Usage: docker-tag-search [flags] <repository name>")
		fmt.Printf("\nflags:\n\n")
		fs.PrintDefaults()
	}
	filter := fs.String("filter", "", "filtering pattern")
	fs.Parse(os.Args[1:])
	args := fs.Args()
	if len(args) == 0 {
		fs.Usage()
		os.Exit(0)
	}
	if err := Search(filter, args); err != nil {
		log.Fatal(err)
	}
}
